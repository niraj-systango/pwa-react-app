import React from 'react';
import { Link } from 'react-router-dom';

import { userService } from '../services/user.service.js';

class HomePage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
        user: {},
        users: []
    };
  }

  componentDidMount() {
    this.setState({ 
        user: JSON.parse(localStorage.getItem('user')),
        users: { loading: true }
    });
    if(navigator.onLine) {
      userService.getAll().then(users => {
        this.setState({ users })
        localStorage.setItem('users', JSON.stringify(users))
      })
    }
    else {
       this.setState({users: JSON.parse(localStorage.getItem('users'))})
    }
  }

  render() {
    const { user, users } = this.state;
    return (
      <div className="col-md-6 col-md-offset-3">
        <h1>Hi {user.firstName}!</h1>
        <p>You're logged in with React & Basic HTTP Authentication!!</p>
        <h3>List of all users</h3>
        {users.loading && <em>Loading users...</em>}
        {users.length &&
          <ul>
              {users.map((user, index) =>
                  <li key={user._id}>
                      {user.name}
                  </li>
              )}
          </ul>
        }
        <p>
          <Link to="/login">Logout</Link>
        </p>
      </div>
    );
  }
}

export { HomePage };