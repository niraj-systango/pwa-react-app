import React from 'react';
import './App.css';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { HomePage } from './home/home_page.js';
import { LoginPage } from './user/login_page.js';
import { PrivateRoute } from './components/PrivateRoute';


export default class App extends React.Component {
  render() {
    return (
      <div className="container">
        <div className="col-sm-8 col-sm-offset-2">
          <Router>
            <div>
              <PrivateRoute exact path="/" component={HomePage} />
              <Route path="/login" component={LoginPage} />
            </div>
          </Router>
        </div>
      </div>
    );
  }
}
